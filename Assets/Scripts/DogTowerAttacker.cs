using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DogTowerAttacker : Mammal
{
    new float health = 12.0f;
    new float speed = 1.0f;
    [SerializeField] new float detectionRange = 3.0f;
    [SerializeField] new float attackRange = 1.0f;
    [SerializeField] new float unitDamage = 0.3f;
    [SerializeField] new float towerDamage = 2.0f;
    new public int cost = 20;
    new public bool attacksMammals = false;
}
