using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EndScreen : MonoBehaviour
{
    public Image endResultImage;
    [SerializeField] Sprite catWinSprite;
    [SerializeField] Sprite catLoseSprite;
    [SerializeField] Sprite dogWinSprite;
    [SerializeField] Sprite dogLoseSprite;

    public void SetText(bool didWin)
    {
        if (didWin)
        {
            if (MatchController.playerSide == UnitType.CAT)
                endResultImage.sprite = catWinSprite;
            else
                endResultImage.sprite = dogWinSprite;
        } else
        {
            if (MatchController.playerSide == UnitType.CAT)
                endResultImage.sprite = catLoseSprite;
            else
                endResultImage.sprite = dogLoseSprite;
        }
    }

    public void QuitGame()
    {
        CurrentMenu.currentMenu = CurrentMenu.menu.MAIN;
        SceneManager.LoadScene(0);
    }

    public void ReplayGame()
    {
        CurrentMenu.currentMenu = CurrentMenu.menu.SELECTION;
        SceneManager.LoadScene(0);
    }
}
