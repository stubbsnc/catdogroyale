using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class DragAndDrop : MonoBehaviour, IDragHandler, IEndDragHandler, IBeginDragHandler
{
    private Vector3 initialPosition;
    private GameObject clone;
    public Mammal unit;
    [SerializeField] Image cardImage;
    [SerializeField] Image currencyImage;
    [SerializeField] Image unitImage;
    [SerializeField] TextMeshProUGUI unitCost;
    [SerializeField] TextMeshProUGUI unitName;
    [SerializeField] TextMeshProUGUI unitDescription;
    [SerializeField] Sprite catBackground;
    [SerializeField] Sprite dogBackground;
    [SerializeField] Sprite catCurrency;
    [SerializeField] Sprite dogCurrency;
    [HideInInspector] public string raycastLayer;
    [SerializeField] AudioClip cardPickUpSFX;
    [SerializeField] AudioClip cardPlaceSFX;
    [SerializeField] AudioClip cardErrorSFX;

    // Start is called before the first frame update
    void Start()
    {
        initialPosition = transform.position;
    }

    public void SetUnit(Mammal unit)
    {
        if (unit.unitType == UnitType.CAT)
        {
            cardImage.sprite = catBackground;
            currencyImage.sprite = catCurrency;
        }
        else
        {
            cardImage.sprite = dogBackground;
            currencyImage.sprite = dogCurrency;
        }
        unitCost.text = unit.cost.ToString();
        unitName.text = unit.unitName;
        unitDescription.text = unit.unitDescription;
        unitImage.sprite = unit.unitImage;
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            transform.position = new Vector3(Input.mousePosition.x, Input.mousePosition.y, transform.position.z);
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            var layer = (Tutorial.instance.state == Tutorial.TutorialState.First_Unit || Tutorial.instance.state == Tutorial.TutorialState.Kill_Reward) ? "TutorialGround" : raycastLayer;
            bool abort = MatchController.doTutorial && Tutorial.instance.state < Tutorial.TutorialState.Kill_Reward && Tutorial.instance.state != Tutorial.TutorialState.First_Unit;
            if (Physics.Raycast(ray, out hit, 100, LayerMask.GetMask(layer)) && !abort)
            {
                AudioManager.PlaySound(cardPlaceSFX, AudioManager.Channel.SFX);
                Mammal newUnit = Instantiate(unit, hit.point, Quaternion.identity);
                MatchController.instance.objects.Add(newUnit);
                newUnit.InitHealth();
                newUnit.setEnemyTowers();
                newUnit.ownerCurrency = MatchController.instance.playerCurrency;
                    MatchController.instance.playerCurrency.SubtractCurrency(unit.cost);
                if ((Tutorial.instance.state == Tutorial.TutorialState.First_Unit || Tutorial.instance.state == Tutorial.TutorialState.Kill_Reward))
                {
                    Tutorial.instance.AdvanceTutorial();
                }
            }
            else
            {
                AudioManager.PlaySound(cardErrorSFX, AudioManager.Channel.SFX);
            }
            transform.position = initialPosition;
            Destroy(clone);
        }
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            clone = Instantiate(gameObject, transform.parent.GetComponent<Transform>());
            clone.transform.position = initialPosition;
            clone.GetComponent<DragAndDrop>().enabled = false;
            AudioManager.PlaySound(cardPickUpSFX, AudioManager.Channel.SFX);
        }
    }

    private void Update()
    {
        bool canBuy = unit.cost <= MatchController.instance.playerCurrency.currency;
        cardImage.raycastTarget = canBuy;
        cardImage.color = canBuy ? Color.white : Color.gray;
    }
}
