using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrentMenu : MonoBehaviour
{
    public enum menu
    {
        MAIN,
        SELECTION
    }
    public static menu currentMenu = menu.MAIN;
}
