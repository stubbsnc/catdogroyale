using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CurrencyUI : MonoBehaviour
{
    public CurrencyAPI currencyAPI;
    public TextMeshProUGUI text;

    // Update is called once per frame
    void Update()
    {
        setCurrencyText();
    }

    void setCurrencyText()
    {
        text.text = $"{currencyAPI.currency}";
    }
}
