using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealingAura : MonoBehaviour
{
    public float healRadius = 5.0f;
    public float healPerSecond = 0.25f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    void healAlliesInRadius()
    {
        foreach (var obj in MatchController.instance.objects)
        {
            if (obj.unitType == UnitType.CAT && inHealingRange(obj.transform.position))
            {
                obj.heal(healPerSecond * Time.deltaTime);
            }
        }
    }

    bool inHealingRange(Vector3 position)
    {
        return Vector3.Distance(transform.position, position) <= healRadius;
    }

    // Update is called once per frame
    void Update()
    {
        healAlliesInRadius();
    }
}
