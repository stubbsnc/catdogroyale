using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MammalDuplicator : MonoBehaviour
{
    public Transform[] clonePositions;
    bool isOriginal = true;
    // Start is called before the first frame update
    void Start()
    {
        if (isOriginal)
        {
            foreach (var spawnPoint in clonePositions)
            {
                GetComponent<Mammal>().cost = Mathf.Max(1, Mathf.RoundToInt(GetComponent<Mammal>().cost / 5f));
                var clone = Instantiate(this, spawnPoint.position, transform.rotation);
                clone.isOriginal = false;
                MatchController.instance.objects.Add(clone.GetComponent<Mammal>());
            }
        }
    }
}
