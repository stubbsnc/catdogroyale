using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour
{
    public List<Transform> spawnPoints = new List<Transform>();
    public List<Mammal> possibleUnits;
    [SerializeField] CurrencyAPI currency;
    [HideInInspector] public Mammal nextUnit;

    Mammal ChooseNextUnit()
    {
        return possibleUnits[Random.Range(0, possibleUnits.Count)];
    }

    Transform chooseSpawnPoint() {
        int index = Random.Range(0, spawnPoints.Count);
        return spawnPoints[index];
    }

    [ContextMenu("placeEnemyUnit")]
    void placeEnemyUnit()
    {
        Transform spawnPoint = chooseSpawnPoint();
        createEnemyUnit(spawnPoint);
        nextUnit = null;
    }

    public Mammal createEnemyUnit(Transform spawnPoint)
    {
        Mammal newUnit = Instantiate(nextUnit, spawnPoint.position, Quaternion.identity);
        MatchController.instance.objects.Add(newUnit);
        newUnit.InitHealth();
        newUnit.setEnemyTowers();
        newUnit.ownerCurrency = currency;
        return newUnit;
    }

    private void Update()
    {
        if (!MatchController.doTutorial)
        {
            if (nextUnit == null)
            {
                nextUnit = ChooseNextUnit();
            }
            if (nextUnit.cost <= currency.currency)
            {
                currency.SubtractCurrency(nextUnit.cost);
                placeEnemyUnit();
            }
        }
    }
}
