using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Mammal : MonoBehaviour
{
    public string unitName;
    public string unitDescription;
    public TowerController tower1;
    public TowerController tower2;
    public float speed = 2.0f;
    [SerializeField] protected float health = 10;
    protected float maxHealth;
    public UnitType unitType;
    public float detectionRange;
    public float attackRange;
    public float unitDamage;
    public float towerDamage;
    public int cost;
    public float goldMultiplier = 1f;
    public CurrencyAPI ownerCurrency;
    public GameObject healthBarRoot;
    public Image healthBarImg;
    public ParticleBurster spawnVFX;
    public float spawnMoveDelay = 0.3f;
    public bool attacksMammals = true;
    public Transform targetTransform;
    public Sprite unitImage;
    [SerializeField] Animator animator;
    [Header("Sound")]
    [SerializeField] AudioClip spawnSound;
    [SerializeField] AudioClip attackSound;
    [SerializeField] AudioClip dieSound;
    [SerializeField] AudioClip hitSound;
    float attackSoundCD = 1.3f;
    float currentAttackSoundCD = 0f;

    private void Start()
    {
        maxHealth = health;
        healthBarRoot.SetActive(false);

        if (spawnSound != null)
            AudioManager.PlaySoundAt(spawnSound, AudioManager.Channel.SFX, transform.position);

        if (spawnVFX == null)
        {
            spawnVFX = Resources.Load<ParticleBurster>("SpawnFX");
        }
        Instantiate(spawnVFX, transform.position, Quaternion.identity).Trigger(true);
    }

    public void Attack(Mammal enemy)
    {
        if (!enemy.isDead())
        {
            enemy.health -= unitDamage * Time.deltaTime;
            float ratio = enemy.health / enemy.maxHealth;
            enemy.healthBarRoot.SetActive(true);
            enemy.healthBarImg.fillAmount = ratio;

            if (enemy.CheckIfDead())
            {
                ownerCurrency.AddCurrency(ComputeReward(enemy));
            }
        }
    }

    int ComputeReward(Mammal victim)
    {
        return Mathf.FloorToInt(victim.cost * goldMultiplier / 2f);
    }

    public void AttackTower(TowerController targetTower)
    {
        targetTower.reduceHealth(towerDamage * Time.deltaTime);
    }

    public void setEnemyTowers()
    {
        if (unitType == UnitType.DOG) {
            tower1 = MatchController.instance.catTowers[0];
            tower2 = MatchController.instance.catTowers[1];
        } else {
            tower1 = MatchController.instance.dogTowers[0];
            tower2 = MatchController.instance.dogTowers[1];
        }
    }

    public void heal(float heal)
    {
        health = Mathf.Min(health + heal, maxHealth);
        healthBarRoot.SetActive(health >= maxHealth);
        healthBarImg.fillAmount = health / maxHealth;
    }

    public void findPath() {
        float step = speed * Time.deltaTime;
        var target = GetNearestEnemy();
        if (target != null)
        {
            transform.LookAt(FlattenY(target.transform.position));
            if (!InRangeOfTarget(target.transform.position))
            {
                animator.SetBool("Attacking", false);
                targetTransform = null;
                var targetUnitPosition = target.transform.position;
                transform.position = Vector3.MoveTowards(transform.position,
                    new Vector3(targetUnitPosition.x, transform.position.y, targetUnitPosition.z), step);
            }
            else
            {
                if (attackSound != null && currentAttackSoundCD <= 0f)
                {
                    AudioManager.PlaySoundAt(attackSound, AudioManager.Channel.SFX, transform.position);
                    currentAttackSoundCD = attackSoundCD;
                }
                animator.SetBool("Attacking", true);
                targetTransform = target.transform;
                Attack(target);
            }
        }
        else
        {
            // finds closest tower
            TowerController targetTower = GetTowerTarget();
            Vector3 targetTowerPosition = targetTower.transform.position;
            transform.LookAt(FlattenY(targetTower.transform.position));
            if (!InRangeOfTarget(targetTower.transform.position))
            {
                animator.SetBool("Attacking", false);
                targetTransform = null;
                transform.position = Vector3.MoveTowards(transform.position,
                    new Vector3(targetTowerPosition.x, transform.position.y, targetTowerPosition.z), step);
            }
            else
            {
                if (hitSound != null && currentAttackSoundCD <= 0f)
                {
                    AudioManager.PlaySoundAt(hitSound, AudioManager.Channel.SFX, transform.position);
                    currentAttackSoundCD = attackSoundCD;
                }
                animator.SetBool("Attacking", true);
                targetTransform = targetTower.transform;
                AttackTower(targetTower);
            }
        }
    }

    Mammal GetNearestEnemy()
    {
        float minDist = float.MaxValue;
        Mammal nearest = null;
        foreach (var mammal in MatchController.instance.objects)
        {
            if (mammal.unitType != unitType && !mammal.isDead() && attacksMammals)
            {
                var distance = (transform.position - mammal.transform.position).sqrMagnitude;
                if (distance < minDist)
                {
                    minDist = distance;
                    nearest = mammal;
                }
            }
        }
        if (nearest != null)
        {
            if (minDist <= Mathf.Pow(detectionRange, 2))
                return nearest;
            else
                return null;
        }
        return null;
    }

    public TowerController GetTowerTarget() {
        float distToTower1 = Vector3.Distance(
            FlattenY(transform.position), 
            FlattenY(tower1.transform.position));
        float distToTower2 = Vector3.Distance(
            FlattenY(transform.position),
            FlattenY(tower2.transform.position));
        if ((distToTower1 < distToTower2 && tower1.isAlive()) || !tower2.isAlive()) {
            return tower1;
        } else {
            return tower2;
        }
    }

    public bool InRangeOfTarget(Vector3 targetPosition) 
    {
        float distance = FlattenY(transform.position - targetPosition).sqrMagnitude;
        return distance <= Mathf.Pow(attackRange, 2);
    }

    Vector3 FlattenY(Vector3 vector)
    {
        return new Vector3(vector.x, 0, vector.z);
    }

    public bool CheckIfDead() 
    {
        if (isDead() && MatchController.instance.objects.Contains(this))
        {
            if (dieSound != null)
                AudioManager.PlaySoundAt(dieSound, AudioManager.Channel.SFX, transform.position);
            healthBarRoot.SetActive(false);
            animator.SetTrigger("Die");
            StartCoroutine(Die());
            MatchController.instance.objects.Remove(this);
        }
        return isDead();
    }

    IEnumerator Die()
    {
        yield return new WaitForSeconds(1.2f);
        float distTraveled = 0f;
        while (distTraveled < 0.5f)
        {
            transform.Translate(new Vector3(0, -0.8f * Time.deltaTime, 0));
            distTraveled += 0.8f * Time.deltaTime;
            yield return null;
        }
        gameObject.SetActive(false);
    }

    public bool isDead()
    {
        return health <= 0;
    }

    private void Update()
    {
        if (!isDead())
        {
            if (currentAttackSoundCD > 0f)
                currentAttackSoundCD -= Time.deltaTime;
            if (spawnMoveDelay <= 0f)
                findPath();
            else
                spawnMoveDelay -= Time.deltaTime;
        }
    }

    public void InitHealth()
    {
        maxHealth = health;
    }
}
