using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DogMelee : Mammal
{
    new float health = 15.0f;
    new public float speed = 2.0f;
    [SerializeField] new float detectionRange = 3.5f;
    [SerializeField] new float attackRange = 1.0f;
    [SerializeField] new float unitDamage = 2.5f;
    [SerializeField] new float towerDamage = 1.0f;
    new public int cost = 10;
}
