using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsPanelManager : MonoBehaviour
{
    [SerializeField] Slider masterSlider;
    [SerializeField] Slider musicSlider;
    [SerializeField] Slider sfxSlider;

    public System.Action onApplyCallback;

    private void Awake()
    {
        Settings.LoadSettings();
        if (masterSlider != null)
            masterSlider.value = Settings.MasterVolume;
        if (musicSlider != null)
            musicSlider.value = Settings.MusicVolume;
        if (sfxSlider != null)
            sfxSlider.value = Settings.SFXVolume;
    }

    public void OnMasterSliderChanged(Slider slider)
    {
        Settings.MasterVolume = slider.value;
    }
    public void OnMusicSliderChanged(Slider slider)
    {
        Settings.MusicVolume = slider.value;
    }
    public void OnSFXSliderChanged(Slider slider)
    {
        Settings.SFXVolume = slider.value;
    }
    public void OnApplyPressed()
    {
        Settings.SaveSettings();
        if (onApplyCallback != null)
            onApplyCallback.Invoke();
        gameObject.SetActive(false);
    }
}
