using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrencyAPI : MonoBehaviour
{
    public int currency = 100;
    public float TimeIntervalBeforeAddingCurrency = 2;
    public int CurrencyPerTimeInterval = 5;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(AddCurrencyOverTime());
    }

    public void AddCurrency(int amount)
    {
        currency += amount;
    }

    public void SetTimeInterval(float seconds)
    {
        TimeIntervalBeforeAddingCurrency = seconds;
    }

    public void SetCurrencyPerInterval(int amount)
    {
        CurrencyPerTimeInterval = amount;
    }

    public void SubtractCurrency(int amount)
    {
        currency -= amount;
        if(currency < 0)
        {
            currency = 0;
        }
    }

    IEnumerator AddCurrencyOverTime()
    {
        while (true)
        {
            yield return new WaitForSeconds(TimeIntervalBeforeAddingCurrency);
            currency += CurrencyPerTimeInterval;
        }
    }
}
