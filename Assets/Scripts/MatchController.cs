using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MatchController : MonoBehaviour
{
    TowerController towerPrefab;

    public TowerController[] catTowers;
    public TowerController[] dogTowers;
    public List<Mammal> objects = new List<Mammal>();
    public static MatchController instance;
    public CurrencyAPI playerCurrency;
    public static UnitType playerSide;
    [SerializeField] List<Mammal> catUnits;
    [SerializeField] List<Mammal> dogUnits;
    [SerializeField] List<Transform> catSpawns;
    [SerializeField] List<Transform> dogSpawns;
    [SerializeField] EnemyAI aiController;
    [SerializeField] DragAndDrop[] playerDeck;
    public EndScreen EndScreenPanel;
    private bool gameOver = false;
    public Image catHealthBar;
    public Image dogHealthBar;
    private float towersMaxHealth = 0;
    public static bool doTutorial;
    [SerializeField] Image currencyIcon;
    [SerializeField] Sprite catCurrencyIcon;
    [SerializeField] Sprite dogCurrencyIcon;
    [Header("Sound")]
    [SerializeField] AudioClip music;
    [SerializeField] AudioClip victoryMusic;
    [SerializeField] AudioClip defeatMusic;
    [SerializeField] AudioClip buttonPressSFX;

    AudioSource musicSource;

    private void Awake()
    {
        InitializeMatch();
    }
    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        
        foreach (TowerController tower in catTowers)
        {
            towersMaxHealth += tower.health;
        }

        musicSource = AudioManager.PlaySound(music, AudioManager.Channel.Music, loop: true);
    }

    void InitializeMatch()
    {
        if (playerSide == UnitType.CAT)
        {
            currencyIcon.sprite = catCurrencyIcon;
            aiController.possibleUnits = dogUnits;
            aiController.spawnPoints = dogSpawns;
            for (int i = 0; i < playerDeck.Length; ++i)
            {
                if (i < catUnits.Count)
                {
                    playerDeck[i].unit = catUnits[i];
                    playerDeck[i].raycastLayer = "CatGround";
                    playerDeck[i].SetUnit(catUnits[i]);
                }
                else
                {
                    playerDeck[i].gameObject.SetActive(false);
                }
            }
            playerDeck[0].unit = catUnits[0];
            playerDeck[0].raycastLayer = "CatGround";
        }
        else
        {
            currencyIcon.sprite = dogCurrencyIcon;
            aiController.possibleUnits = catUnits;
            aiController.spawnPoints = catSpawns;
            for (int i = 0; i < playerDeck.Length; ++i)
            {
                if (i < dogUnits.Count && (!doTutorial || i == 0))
                {
                    playerDeck[i].unit = dogUnits[i];
                    playerDeck[i].raycastLayer = "DogGround";
                    playerDeck[i].SetUnit(dogUnits[i]);
                }
                else
                {
                    playerDeck[i].gameObject.SetActive(false);
                }
            }

        }

        if (doTutorial)
        {
            InitializeTutorial();
        }
    }

    void InitializeTutorial()
    {
        playerCurrency.CurrencyPerTimeInterval = 0;
    }

    private void Update()
    {
        if (!catTowers[0].isAlive() && !catTowers[1].isAlive() && !gameOver && !doTutorial)
        {
            gameOver = true;
            EndScreenPanel.gameObject.SetActive(true);
            if (playerSide == UnitType.DOG)
            {
                EndScreenPanel.SetText(true);
                StartCoroutine(TempSwitchMusic(victoryMusic));
            }
            else
            {
                EndScreenPanel.SetText(false);
                StartCoroutine(TempSwitchMusic(defeatMusic));
            }
        }
        else if (!dogTowers[0].isAlive() && !dogTowers[1].isAlive() && !gameOver && !doTutorial)
        {
            gameOver = true;
            EndScreenPanel.gameObject.SetActive(true);
            if (playerSide == UnitType.CAT)
            {
                EndScreenPanel.SetText(true);
                StartCoroutine(TempSwitchMusic(victoryMusic));
            }
            else
            {
                EndScreenPanel.SetText(false);
                StartCoroutine(TempSwitchMusic(defeatMusic));
            }
        }

        float curCatTowersHealth = 0;
        foreach(TowerController tower in catTowers)
        {
            curCatTowersHealth += tower.health;
        }
        float ratio = curCatTowersHealth / towersMaxHealth;
        catHealthBar.fillAmount = ratio;
        float curDogTowersHealth = 0;
        foreach (TowerController tower in dogTowers)
        {
            curDogTowersHealth += tower.health;
        }
        ratio = curDogTowersHealth / towersMaxHealth;
        dogHealthBar.fillAmount = ratio;
    }

    IEnumerator TempSwitchMusic(AudioClip newMusic)
    {
        musicSource.Stop();
        musicSource = AudioManager.PlaySound(newMusic, AudioManager.Channel.Music);
        while (musicSource.isPlaying)
            yield return null;
        musicSource = AudioManager.PlaySound(music, AudioManager.Channel.Music, loop: true);
    }

    public void PlayButtonSound()
    {
        AudioManager.PlaySound(buttonPressSFX, AudioManager.Channel.SFX);
    }

    [ContextMenu("printObjectList")]
    private void printObjectList()
    {
        Debug.Log(objects);
    }
}
