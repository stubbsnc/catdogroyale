using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoneAnimation : MonoBehaviour
{
    public float turnSpeed;

    private void Update()
    {
        transform.localEulerAngles = new Vector3(0, transform.localEulerAngles.y + turnSpeed * Time.unscaledDeltaTime, 0);
    }
}
