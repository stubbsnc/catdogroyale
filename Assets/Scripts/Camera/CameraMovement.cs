using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CameraMovement : MonoBehaviour
{
    public GameObject pivot;
    public float mouseSensitivity = 1f;

    bool rmbPressed;
    float pannedDistance;

    private void Start()
    {
        pivot.transform.position = new Vector3(0, pivot.transform.position.y, pivot.transform.position.z);
        StartCoroutine(DoCameraControls());
    }

    // Update is called once per frame
    IEnumerator DoCameraControls()
    {
        while (true)
        {
            float factor = 30f * Time.unscaledDeltaTime;
            float rFactor = 600f * Time.unscaledDeltaTime;
            pivot.transform.position = new Vector3(pivot.transform.position.x + (Input.GetAxisRaw("Horizontal") * factor), pivot.transform.position.y, pivot.transform.position.z);
            if (Tutorial.instance.state == Tutorial.TutorialState.Camera_Controls_Pan)
                pannedDistance += Mathf.Abs(Input.GetAxisRaw("Horizontal") * factor);

            if (Input.GetMouseButton(1))
            {
                float x = rFactor * Input.GetAxis("Mouse X") * mouseSensitivity;
                float y = rFactor * Input.GetAxis("Mouse Y") * mouseSensitivity;
                x = x > 50f || x < -50f ? 0f : x;
                y = y > 50f || y < -50f ? 0f : y;
                pivot.transform.Rotate(Vector3.up, x);
                transform.Rotate(Vector3.left, y);

                if (Tutorial.instance.state == Tutorial.TutorialState.Camera_Controls_Swivel)
                {
                    rmbPressed = true;
                }
            }

            if (Input.GetMouseButtonUp(1) && rmbPressed && Tutorial.instance.state == Tutorial.TutorialState.Camera_Controls_Swivel)
            {
                Tutorial.instance.AdvanceTutorial();
            }
            else if (pannedDistance > 15 && Tutorial.instance.state == Tutorial.TutorialState.Camera_Controls_Pan)
            {
                Tutorial.instance.AdvanceTutorial();
            }
            yield return null;
        }
    }
}
