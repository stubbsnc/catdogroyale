using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballMovement : MonoBehaviour
{
    public Transform end;

    // Movement speed in units per second.
    public float speed = 1.0f;

    // Move to the target end position.
    void Update()
    {
        if (end)
        {
            transform.LookAt(end);
            transform.Translate(transform.forward * speed * Time.deltaTime, Space.World);

            if (Vector3.Distance(transform.position, end.position) < .1)
            {
                Destroy(gameObject);
            }
        }
    }
}
