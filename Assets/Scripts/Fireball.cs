using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : MonoBehaviour
{
    public GameObject fireball;
    public Mammal mage;
    private bool attacking = false;
    [SerializeField] AudioClip fireballSFX;

    // Update is called once per frame
    void Update()
    {
        if(!attacking && mage.targetTransform)
        {
            attacking = true;
            StartCoroutine(shootingFirteballs());
        } else if (attacking && !mage.targetTransform)
        {
            attacking = false;
        }
    }

    IEnumerator shootingFirteballs()
    {
        while (mage.targetTransform)
        {
            var fb = Instantiate(fireball, transform.position, transform.rotation);
            fb.transform.LookAt(mage.targetTransform);
            FireballMovement fbMov = fb.GetComponent<FireballMovement>();
            fbMov.end = mage.targetTransform;
            AudioManager.PlaySoundAt(fireballSFX, AudioManager.Channel.SFX, transform.position);
            yield return new WaitForSeconds(1.5f);
        }
        yield return null;
    }
}
