using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Tutorial : MonoBehaviour
{
    public enum TutorialState
    {
        Init,
        Welcome,
        Overview,
        Camera_Controls_Swivel,
        Camera_Controls_Pan,
        First_Unit,
        Explain_Currency,
        Enemy_Spawn,
        Enemy_Kill,
        Kill_Reward,
        Explain_Tower,
        Kill_Tower,
        Explain_Win,
        Kill_Tower_2,
        Win,
        End
    }

    public enum PanePositions
    {
        Center,
        Left
    }

    Vector2 GetPanePosition(PanePositions position)
    {
        switch (position)
        {
            case PanePositions.Center:
                return new Vector2(Screen.width / 2, Screen.height / 2);
            case PanePositions.Left:
                return new Vector2(450, Screen.height / 2);
            default:
                return new Vector2(Screen.width / 2, Screen.height / 2);
        }
    }

    public static Tutorial instance;
    public TutorialState state;
    public GameObject tutorialPopup;
    public TextMeshProUGUI tutorialText;
    public GameObject tutorialNext;
    Mammal tutorialEnemy;
    [SerializeField] AudioClip advanceSound;
    [SerializeField] Transform enemySpawnPoint;
    [SerializeField] EnemyAI enemy;
    [SerializeField] GameObject tutorialGround;
    [SerializeField] Mammal enemyUnit;

    private void Awake()
    {
        instance = this;
        state = TutorialState.Init;
        if (MatchController.doTutorial)
            AdvanceTutorial();
    }
    
    public void AdvanceTutorial()
    {
        state = state + 1;
        tutorialPopup.SetActive(false);

        AudioManager.PlaySound(advanceSound, AudioManager.Channel.SFX);

        switch (state)
        {
            case TutorialState.Welcome:
                Time.timeScale = 0f;
                PlacePopup(PanePositions.Center, "Welcome to name of video game! This quick tutorial will show you the basics. Press 'next' to continue!", true);
                break;
            case TutorialState.Overview:
                PlacePopup(PanePositions.Center, "In this game, cats and dogs have an epic battle! You are playing as the dogs for the tutorial, but you can select your side in the main game.", true);
                break;
            case TutorialState.Camera_Controls_Swivel:
                PlacePopup(PanePositions.Left, "To look around with the camera, hold right click and move your mouse.", false);
                break;
            case TutorialState.Camera_Controls_Pan:
                PlacePopup(PanePositions.Left, "To pan the camera, press A or D or the left and right arrow keys.", false);
                break;
            case TutorialState.First_Unit:
                tutorialGround.SetActive(true);
                PlacePopup(PanePositions.Left, "To place a unit, drag it from your unit bar to anywhere on your side of the field. Try dragging a unit to the indicated circle.", false);
                break;
            case TutorialState.Explain_Currency:
                tutorialGround.SetActive(false);
                PlacePopup(PanePositions.Center, "Placing units costs treats. You can see your current treats in the bottom-left, and unit costs on the unit icon. Your treats will naturally regenerate over time.", true);
                break;
            case TutorialState.Enemy_Spawn:
                StartCoroutine(SpawnTutorialEnemy());
                break;
            case TutorialState.Enemy_Kill:
                StartCoroutine(WaitForEnemyDeath());
                break;
            case TutorialState.Kill_Reward:
                tutorialGround.SetActive(true);
                PlacePopup(PanePositions.Left, "You gain a bit of extra treats whenever you kill an enemy unit. You have enough to spawn another unit now - go ahead and do that!", false);
                break;
            case TutorialState.Explain_Tower:
                tutorialGround.SetActive(false);
                PlacePopup(PanePositions.Center, "With no enemy units in visible range, your units will now advance towards the enemy towers and attempt to destroy them!", true);
                break;
            case TutorialState.Kill_Tower:
                StartCoroutine(WaitForTowerDeath(1));
                break;
            case TutorialState.Explain_Win:
                PlacePopup(PanePositions.Center, "Your units just destroyed an enemy tower! When both towers are destroyed, you win the game!", true);
                break;
            case TutorialState.Kill_Tower_2:
                StartCoroutine(WaitForTowerDeath(0));
                break;
            case TutorialState.Win:
                PlacePopup(PanePositions.Center, "Your units destroyed both towers! You've beaten the tutorial - expect the enemy to put up a little more resistance in the main game!", true);
                break;
            case TutorialState.End:
                Time.timeScale = 1f;
                SceneManager.LoadScene(0);
                break;
        }

    }

    IEnumerator SpawnTutorialEnemy()
    {
        Time.timeScale = 1f;
        yield return new WaitForSeconds(0.5f);
        enemy.nextUnit = enemyUnit;
        tutorialEnemy = enemy.createEnemyUnit(enemySpawnPoint);
        tutorialEnemy.unitDamage = 0.5f;
        yield return new WaitForSeconds(0.5f);
        Time.timeScale = 0f;
        PlacePopup(PanePositions.Center, "The cats just spawned a unit of their own! Fortunately, it's weak, so your unit should be able to win the fight.", true);
    }

    IEnumerator WaitForEnemyDeath()
    {
        Time.timeScale = 1f;
        while (!tutorialEnemy.isDead()) yield return null;
        AdvanceTutorial();
        Time.timeScale = 0f;
    }

    IEnumerator WaitForTowerDeath(int towerID)
    {
        Time.timeScale = 1f;
        while (MatchController.instance.catTowers[towerID].isAlive()) yield return null;
        AdvanceTutorial();
        Time.timeScale = 0f;
    }

    void PlacePopup(PanePositions panePosition, string text, bool showDismissButton)
    {
        tutorialPopup.SetActive(true);
        tutorialPopup.transform.position = GetPanePosition(panePosition);
        tutorialText.text = text;
        tutorialNext.SetActive(showDismissButton);
    }
}
