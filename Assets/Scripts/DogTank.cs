using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DogTank : Mammal
{
    new float health = 30.0f;
    new float speed = 0.8f;
    [SerializeField] new float detectionRange = 5.0f;
    [SerializeField] new float attackRange = 1;
    [SerializeField] new float unitDamage = 0.5f;
    [SerializeField] new float towerDamage = 0.5f;
    new public int cost = 30;
}
