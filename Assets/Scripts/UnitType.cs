using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum UnitType
{
    CAT,
    DOG,
    CAT_TOWER,
    DOG_TOWER
}