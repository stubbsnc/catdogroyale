using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TowerController : MonoBehaviour
{
    public float health = 10;
    private float maxHealth;
    public UnitType unitType;
    public Image healthBarImg;
    [SerializeField] AudioClip towerDestruction;
    [SerializeField] AudioClip towerUnderAttack;
    float towerAlarmCD = 3f;
    float currentTowerAlarmCD = 0f;

    // Start is called before the first frame update
    void Start()
    {
        maxHealth = health;
    }

    // Update is called once per frame
    void Update()
    {
        if (currentTowerAlarmCD > 0f)
            currentTowerAlarmCD -= Time.deltaTime;

        if (!isAlive()) {
            AudioManager.PlaySoundAt(towerDestruction, AudioManager.Channel.SFX, transform.position);
            gameObject.SetActive(false);
        }
    }

    public void reduceHealth(float healthHit) {
        if (health > 0) {
            health -= healthHit;
        }

        if (currentTowerAlarmCD <= 0f && ((unitType == UnitType.CAT_TOWER && MatchController.playerSide == UnitType.CAT) || (unitType == UnitType.DOG_TOWER && MatchController.playerSide == UnitType.DOG)))
        {
            AudioManager.PlaySoundAt(towerUnderAttack, AudioManager.Channel.SFX, transform.position);
            currentTowerAlarmCD = towerAlarmCD;
        }

        float ratio = health / maxHealth;
        healthBarImg.fillAmount = ratio;
    }

    public bool isAlive() 
    {
        if (health > 0) {
            return true;
        }
        return false;
    }
}
