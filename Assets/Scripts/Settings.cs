using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Settings : MonoBehaviour
{
    static bool initialized;

    static float _masterVolume;
    [SerializeField]
    public static float MasterVolume
    {
        get
        {
            return _masterVolume;
        }
        set
        {
            _masterVolume = value;
            AudioManager.SetChannelVolume(AudioManager.Channel.Master, value);
        }
    }
    static float _musicVolume;
    public static float MusicVolume
    {
        get
        {
            return _musicVolume;
        }
        set
        {
            _musicVolume = value;
            AudioManager.SetChannelVolume(AudioManager.Channel.Music, value);
        }
    }
    static float _sfxVolume;
    public static float SFXVolume
    {
        get
        {
            return _sfxVolume;
        }
        set
        {
            _sfxVolume = value;
            AudioManager.SetChannelVolume(AudioManager.Channel.SFX, value);
        }
    }

    public static void LoadSettings()
    {
        if (!initialized)
        {
            initialized = true;
            if (PlayerPrefs.HasKey("MasterVolume"))
            {
                MasterVolume = PlayerPrefs.GetFloat("MasterVolume");
            }
            else
            {
                MasterVolume = 0.7f;
            }
            if (PlayerPrefs.HasKey("MusicVolume"))
            {
                MusicVolume = PlayerPrefs.GetFloat("MusicVolume");
            }
            else
            {
                MusicVolume = 1f;
            }
            if (PlayerPrefs.HasKey("SFXVolume"))
            {
                SFXVolume = PlayerPrefs.GetFloat("SFXVolume");
            }
            else
            {
                SFXVolume = 1f;
            }
        }
    }

    public static void SaveSettings()
    {
        PlayerPrefs.SetFloat("MasterVolume", MasterVolume);
        PlayerPrefs.SetFloat("MusicVolume", MusicVolume);
        PlayerPrefs.SetFloat("SFXVolume", SFXVolume);
    }
}
