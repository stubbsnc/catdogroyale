using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour
{
    [SerializeField] SettingsPanelManager settingsPane;
    [SerializeField] GameObject buttons;
    [SerializeField] AudioClip buttonPressSFX;
    [SerializeField] GameObject sideSelectPane;
    [SerializeField] GameObject creditsPane;

    [SerializeField] AudioClip musicIntro;
    [SerializeField] AudioClip musicLoop;

    private void Awake()
    {
        Settings.LoadSettings();
        settingsPane.onApplyCallback += () =>
        {
            buttons.SetActive(true);
            AudioManager.PlaySound(buttonPressSFX, AudioManager.Channel.SFX);
        };
    }

    private void Start()
    {
        if(CurrentMenu.currentMenu == CurrentMenu.menu.SELECTION)
        {
            sideSelectPane.SetActive(true);
            buttons.SetActive(false);
        }

        StartCoroutine(PlayMusic());
    }

    IEnumerator PlayMusic()
    {
        if (musicIntro != null)
        {
            var source = AudioManager.PlaySound(musicIntro, AudioManager.Channel.Music);
            while (source.isPlaying)
            {
                yield return null;
            }
        }
        AudioManager.PlaySound(musicLoop, AudioManager.Channel.Music, loop: true);
    }

    public void OnPlayPressed()
    {
        MatchController.doTutorial = false;
        AudioManager.PlaySound(buttonPressSFX, AudioManager.Channel.SFX);
        sideSelectPane.SetActive(true);
        buttons.SetActive(false);
    }
    public void OnTutorialPressed()
    {
        AudioManager.PlaySound(buttonPressSFX, AudioManager.Channel.SFX);
        MatchController.doTutorial = true;
        MatchController.playerSide = UnitType.DOG;
        SceneManager.LoadScene(1);
    }
    public void OnSettingsPressed()
    {
        AudioManager.PlaySound(buttonPressSFX, AudioManager.Channel.SFX);
        settingsPane.gameObject.SetActive(true);
        buttons.SetActive(false);
    }
    public void OnExit()
    {
        AudioManager.PlaySound(buttonPressSFX, AudioManager.Channel.SFX);
        Application.Quit();
    }

    public void OnCatSelected() => OnSideSelected(UnitType.CAT);
    public void OnDogSelected() => OnSideSelected(UnitType.DOG);

    public void OnSideSelected(UnitType side)
    {
        AudioManager.PlaySound(buttonPressSFX, AudioManager.Channel.SFX);
        MatchController.doTutorial = false;
        AudioManager.PlaySound(buttonPressSFX, AudioManager.Channel.SFX);
        MatchController.playerSide = side;
        SceneManager.LoadScene(1);
    }

    public void OnCreditsPressed()
    {
        creditsPane.SetActive(true);
        buttons.SetActive(false);
    }

    public void OnCreditsBackPressed()
    {
        creditsPane.SetActive(false);
        buttons.SetActive(true);
    }
}
