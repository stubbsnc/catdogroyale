using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleBurster : MonoBehaviour
{
    [SerializeField] ParticleSystem[] systems;

    public void Trigger(bool destroyOnFinish)
    {
        foreach (var system in systems)
        {
            system.Play();
        }

        if (destroyOnFinish)
        {
            StartCoroutine(DestroyWhenFinished());
        }
    }

    IEnumerator DestroyWhenFinished()
    {
        while (true)
        {
            bool done = true;
            foreach (var system in systems)
            {
                if (system.isPlaying)
                    done = false;
            }
            if (done)
                break;
            yield return null;
        }
        Destroy(gameObject);
    }
}
