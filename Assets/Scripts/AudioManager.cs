using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class AudioManager : MonoBehaviour
{
    public enum Channel
    {
        Master,
        Music,
        SFX
    }

    static AudioMixer _mixer;
    static AudioMixer Mixer
    {
        get
        {
            if (_mixer == null)
            {
                _mixer = Resources.Load<AudioMixer>("AudioMixer");
            }
            return _mixer;
        }
    }

    static Stack<AudioSource> recycledSources;
    static AudioSource _sourcePrefab;
    static AudioSource SourcePrefab
    {
        get
        {
            if (_sourcePrefab == null)
            {
                _sourcePrefab = Resources.Load<AudioSource>("AudioPlayerPrefab");
            }
            return _sourcePrefab;
        }
    }

    static string ChannelToString(Channel channel)
    {
        switch(channel)
        {
            case Channel.Master: return "Master";
            case Channel.Music: return "Music";
            case Channel.SFX: return "SFX";
        }

        return "wtf";
    }

    static AudioMixerGroup[] _audioMixers;
    static AudioMixerGroup GetMixerGroup(Channel channel)
    {
        if (_audioMixers == null)
        {
            _audioMixers = new AudioMixerGroup[3];
            _audioMixers[(int)Channel.Master] = Mixer.FindMatchingGroups(ChannelToString(Channel.Master))[0];
            _audioMixers[(int)Channel.Music] = Mixer.FindMatchingGroups(ChannelToString(Channel.Music))[0];
            _audioMixers[(int)Channel.SFX] = Mixer.FindMatchingGroups(ChannelToString(Channel.SFX))[0];
        }

        return _audioMixers[(int)channel];
    }

    static float RemapVolume(float volume) => Mathf.Log10(volume) * 20;


    public static void SetChannelVolume(Channel channel, float volume)
    {
        volume = Mathf.Clamp(volume, 0.00001f, 1f);
        GetMixerGroup(channel).audioMixer.SetFloat(channel.ToString() + "Volume", RemapVolume(volume));
    }

    public static AudioSource PlaySound(AudioClip sound, Channel channel, float pitchVariance = 0f, bool loop = false)
    {
        var source = GetNewSource();
        source.clip = sound;
        source.volume = 1f;
        source.pitch = 1f + Random.Range(-pitchVariance, pitchVariance);
        source.loop = loop;
        source.spatialBlend = 0f;
        source.outputAudioMixerGroup = GetMixerGroup(channel);
        source.Play();
        return source;
    }

    public static AudioSource PlaySoundAt(AudioClip sound, Channel channel, Vector3 position, float pitchVariance = 0f, bool loop = false)
    {
        var source = GetNewSource();
        source.clip = sound;
        source.volume = 1f;
        source.pitch = 1f + Random.Range(-pitchVariance, pitchVariance);
        source.loop = loop;
        //source.spatialBlend = 1f;
        source.transform.position = position;
        source.outputAudioMixerGroup = GetMixerGroup(channel);
        source.Play();
        return source;
    }

    public static AudioSource PlaySoundOn(AudioClip sound, Channel channel, Transform parent, float pitchVariance = 0f, bool loop = false)
    {
        var source = GetNewSource();
        source.clip = sound;
        source.volume = 1f;
        source.pitch = 1f + Random.Range(-pitchVariance, pitchVariance);
        source.loop = loop;
        //source.spatialBlend = 1f;
        source.transform.parent = parent;
        source.transform.localPosition = Vector3.zero;
        source.outputAudioMixerGroup = GetMixerGroup(channel);
        source.Play();
        return source;
    }

    static void ClearRecycledSources()
    {
        if (recycledSources == null)
            SceneManager.sceneLoaded += (_, __) => ClearRecycledSources();

        Debug.Log("Clearing recycled audio sources...");
        recycledSources = new Stack<AudioSource>();
    }

    static AudioSource GetNewSource()
    {
        if (recycledSources == null || recycledSources.Count == 0)
            return Instantiate(SourcePrefab);

        var source = recycledSources.Pop();
        source.gameObject.SetActive(true);
        return source;
    }

    public static void Recycle(AudioSource source)
    {
        if (recycledSources == null)
            ClearRecycledSources();

        source.Stop();
        source.pitch = 1f;
        source.transform.parent = null;
        source.gameObject.SetActive(false);
        recycledSources.Push(source);
    }
}
